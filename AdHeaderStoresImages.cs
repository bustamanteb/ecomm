﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eComm
{
    class AdHeaderStoresImages : AdHeader
    {
        private string adID;
        private string storeID;
        private string imageURL;
        private string imageName;

        public AdHeaderStoresImages(string adID, string storeID, string imageURL, string imageName) : base(adID)
        {
            this.adID = adID;
            this.storeID = storeID;
            this.imageURL = imageURL;
            this.imageName = imageName;
        }

        public string AdID { get { return adID; } set { adID = value; } }
        public string StoreID { get { return storeID; } }
        public string ImageURL { get { return imageURL; } }
        public string ImageName { get { return imageName; } }

        public string saveAdData(string clientDB)
        {
            MySQL mySql = new MySQL(clientDB);
            string connectionString = mySql.mssqlConnectionString(clientDB);
            CMS_DB cmsDB = new CMS_DB(connectionString);

            DataSet newAdHeaderStoreImages = cmsDB.saveAdHeaderStoresImages_eComm(AdID, StoreID, ImageURL, ImageName);

            if (newAdHeaderStoreImages != null)
                return "1";
            else
                return "0";
        }

        public string saveAdData(string clientUsername, string clientPw)
        {
            RestClient client = new RestClient("https://cloud.brdata.com/api/Ecomm/");
            //RestClient client = new RestClient("http://saastest.brdata.com/api/Ecomm/");
            //RestClient client = new RestClient("http://localhost:57484/api/eComm/");        

            string obj = "{\"BaseParams\": [{\"username\": \"" + clientUsername + "\",\"password\": \"" + clientPw + "\",\"query\": \"" + "adHeaderStoresImages" + "\"}]," +
                           "\"QueryParams\": [{" +
                                "\"adID\": \"" + AdID + "\"," +
                                "\"storeID\": \"" + StoreID + "\"," +
                                "\"imageURL\": \"" + ImageURL + "\"," +
                                "\"imageName\": \"" + ImageName + "\"" +
                                "}]}";

            RestRequest request = new RestRequest();//("Products", Method.GET);
            request.Method = Method.POST;
            request.AddJsonBody(obj);
            request.Timeout = 30000;// Timeout.Infinite;
            var result = client.Execute(request);

            if (result.StatusCode.ToString() == "OK")
            {
                string value = result.Content;//Sample: {"adHeaderID":173}

                JObject adIDJson = JObject.Parse(value);
                string adID = adIDJson["adHeaderStoreImgSuccess"] == null ? "" : adIDJson["adHeaderStoreImgSuccess"].ToString();//JsonConvert.DeserializeObject<string>(@"{""adHeaderID"":173}");

                return adID;
            }
            else
                return "0";
        }
    }
}
