﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace eComm
{
    class MySQL
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string user;
        private string password;

        public MySQL(string database)
        {
            this.database = database;
            Initialize();
        }

        private void Initialize()
        {
            //Old MySQL DB
            //server = "23.253.121.6";
            //New MySQLDB
            server = "brdata-mysql1.mysql.database.azure.com";
            database = "BRdata";            
            //user = "administrator";
            user = "cloudadmin@brdata-mysql1";
            password = "sX*@DuJ8ct";

            string connectionString;
            connectionString = "SERVER=" + server + ";" +
                               "DATABASE=" + database + ";" +
                               "UID=" + user + ";" +
                               "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);
        }

        public string mssqlConnectionString(string database)
        {
            //string query = "SELECT datasource.database, datasource.server " +
            //               "FROM datasource " +
            //               "WHERE datasource.database = '" + database + "'";
            //string server;
            //if (this.OpenConnection() == true)
            //{
            //    MySqlCommand command = new MySqlCommand(query, connection);
            //    MySqlDataReader dataReader = command.ExecuteReader();
            //    dataReader.Read();
            //    server = dataReader["server"].ToString();
            //    dataReader.Close();
            //    this.CloseConnection();
            //    return "Server=" + server + ";" +
            //           "Initial Catalog=" + database + ";" +
            //           "User ID=BRdataSQLAdmin;" +
            //           "Password=trsbB!8yjtGA$Nt;";
            string connectionString;
            //Old Main DB Server
            //return connectionString = "Server=104.130.156.56;" +
            //New, AFS DB Server
            return connectionString = "Server=23.96.21.195;" +
                       "Initial Catalog=MaceysCMS;" +
                       "User ID=BRdataSQLAdmin;" +
                       "Password=trsbB!8yjtGA$Nt;";//
            //}
            //else
            //{
            //    return null;
            //}
        }

        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException exception)
            {
                //catch errors
                return false;
            }
        }

        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException exception)
            {
                // catch errors
                return false;
            }
        }
    }
}
