﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eComm
{
    class AdHeaderZip
    {
        private string zipFile;
        private string zipFilePath;
        private string zipTargetFilePath;

        public AdHeaderZip(string zipFile, string zipFilePath, string zipTargetFilePath)
        {
            this.zipFile = zipFile;
            this.zipFilePath = zipFilePath;
            this.zipTargetFilePath = zipTargetFilePath;
        }

        public string ZipFile { get { return zipFile; } }
        public string ZipFilePath { get { return zipFilePath; } }
        public string ZipTargetFilePath { get { return zipTargetFilePath; } }

        public void moveZipFile()
        {
            // To copy a folder's contents to a new location:
            // Create a new target folder, if necessary.
            if (!System.IO.Directory.Exists(zipTargetFilePath))
            {
                System.IO.Directory.CreateDirectory(zipTargetFilePath);
            }

            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.Combine(ZipFilePath, ZipFile);
            string destFile = System.IO.Path.Combine(ZipTargetFilePath, ZipFile);

            // move zip file to working folder
            System.IO.File.Move(sourceFile, destFile);
        }

        public void unzipImages()
        {
            string destFile = System.IO.Path.Combine(ZipTargetFilePath, ZipFile);
            System.IO.Compression.ZipFile.ExtractToDirectory(destFile, ZipTargetFilePath);
        }

        public Dictionary<string, string> getImageNameAndPath()
        {            
            List<string> allowedImgTypes = new List<string>(); //List holding allowed image types
            allowedImgTypes.Add(".jpg");
            allowedImgTypes.Add(".jpeg");
            allowedImgTypes.Add(".gif");
            allowedImgTypes.Add(".bmp");
            allowedImgTypes.Add(".png");
            
            Dictionary<string, string> imgDictionary = new Dictionary<string, string>();

            string[] imgFiles = Directory.GetFiles(ZipTargetFilePath);

            string filename = "";

            //Get and store all the images in the folder
            foreach (var imgPath in imgFiles)
            {
                filename = Path.GetFileName(imgPath);

                if (allowedImgTypes.Contains(Path.GetExtension(filename))) //Verify that only images are saved to dictionary
                    imgDictionary.Add(filename, imgPath); //Saves image name and image path
            }

            return imgDictionary;
        }

        public void moveZipImgToArchive(string archiveDir)
        {
            // To move a folder's contents to a new location:
            // Create a new target folder, if necessary.
            if (!System.IO.Directory.Exists(archiveDir))
            {
                System.IO.Directory.CreateDirectory(archiveDir);
            }

            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.Combine(ZipTargetFilePath, zipFile);
            string destFile = System.IO.Path.Combine(archiveDir, zipFile);

            // move zip file from working folder to archive folder
            System.IO.File.Move(sourceFile, destFile);
        }
    }
}
