﻿using System.Collections.Generic;
using net.openstack.Providers.Rackspace;
using net.openstack.Core.Domain;
using System.IO;


namespace eComm
{
    public class CloudFiles
    {
        private string username { get; set; }
        private string password { get; set; }
        private CloudIdentity cloudIdentity { get; set; }
        private CloudIdentityProvider cloudIdentityProvider { get; set; }
        private CloudFilesProvider cloudFilesProvider { get; set; }

        public CloudFiles(string username = null, string password = null)
        {
            this.username = username;
            this.password = password;
            initialize();
        }

        private void initialize()
        {
            cloudIdentity = new CloudIdentity()
            {
                Username = username,
                APIKey = password
            };
            cloudIdentityProvider = new CloudIdentityProvider(cloudIdentity);
            cloudFilesProvider = new CloudFilesProvider(cloudIdentity);
        }

        public void createContainer(string container_name)
        {
            cloudFilesProvider.CreateContainer(container_name, region: "DFW");
        }

        public Dictionary<string, string> inspectContainer(string container_name)
        {
            return cloudFilesProvider.GetContainerHeader(container_name, "DFW");
        }

        public Dictionary<string, string> makeContainerPublic(string container_name)
        {
            long timeToLive = 604800;
            return cloudFilesProvider.EnableCDNOnContainer(container_name, timeToLive);
        }

        public Dictionary<string, string> makeContainerPublic(string container_name, long timeToLive)
        {
            return cloudFilesProvider.EnableCDNOnContainer(container_name, timeToLive);
        }

        public void makeContainerPrivate(string container_name)
        {
            cloudFilesProvider.DisableCDNOnContainer(container_name);
        }

        public void deleteContainer(string container_name)
        {
            // From Rackspace: For data safety reasons, you may not delete a container until all objects within it have been deleted.
            cloudFilesProvider.DeleteContainer(container_name);
        }

        public void uploadObject(string path_to_file, string container_name, string object_name)
        {
            using (Stream fileStream = File.OpenRead(path_to_file))
            {
                cloudFilesProvider.CreateObject(container_name, fileStream, object_name, region: "DFW");
            }
        }

        public void uploadObject(Stream fileStream, string container_name, string object_name)
        {
            cloudFilesProvider.CreateObject(container_name, fileStream, object_name, region: "DFW");
        }

        public IEnumerable<Container> listContainers()
        {
            IEnumerable<Container> containerList = cloudFilesProvider.ListContainers(region: "DFW");

            return containerList;
        }

        public IEnumerable<ContainerObject> listObjects(string container_name)
        {
            IEnumerable<ContainerObject> containerObjectList = cloudFilesProvider.ListObjects(container_name, region: "DFW");
            return containerObjectList;
        }

        public string objectUrl(string container_name, string object_name)
        {
            ContainerCDN container = cloudFilesProvider.GetContainerCDNHeader(container: container_name, region: "DFW");
            return container.CDNUri + "/" + object_name;
            //string urlForHTTPS = container.CDNSslUri;
            //string urlForiOSStreaming = container.CDNIosUri;
            //string urlForStreaming = container.CDNStreamingUri;
        }

        public void deleteObject(string container_name, string object_name)
        {
            cloudFilesProvider.DeleteObject(container_name, object_name);
        }
    }
}
