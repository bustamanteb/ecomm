﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eComm
{
    class AdHeaderStores : AdHeader
    {
        private string adID;
        private string storeID;
        private string eCommXML;

        public AdHeaderStores(string adID, string storeID, string eCommXML) : base(adID)
        {
            this.adID = adID;
            this.storeID = storeID;
            this.eCommXML = eCommXML;
        }

        public string AdID { get { return adID; } set { adID = value; } }
        public string StoreID { get { return storeID; } }
        public string ECommXML { get { return eCommXML; } }

        public string saveAdData(string clientDB)
        {
            MySQL mySql = new MySQL(clientDB);
            string connectionString = mySql.mssqlConnectionString(clientDB);
            CMS_DB cmsDB = new CMS_DB(connectionString);

            DataSet newAdHeaderStore = cmsDB.saveAdHeaderStores_eComm(AdID, StoreID, ECommXML);

            if (newAdHeaderStore != null)
                return "1";
            else
                return "0";
        }

        public string saveAdData(string clientUsername, string clientPw)
        {
            RestClient client = new RestClient("https://cloud.brdata.com/api/Ecomm/");
            //RestClient client = new RestClient("http://saastest.brdata.com/api/Ecomm/");
            //RestClient client = new RestClient("http://localhost:57484/api/eComm/");        

            string obj = "{\"BaseParams\": [{\"username\": \"" + clientUsername + "\",\"password\": \"" + clientPw + "\",\"query\": \"" + "adHeaderStores" + "\"}]," +
                           "\"QueryParams\": [{" +
                                "\"adID\": \"" + AdID + "\"," +
                                "\"storeID\": \"" + StoreID + "\"," +
                                "\"eCommXML\": \"" + ECommXML + "\"" +
                                "}]}";

            RestRequest request = new RestRequest();//("Products", Method.GET);
            request.Method = Method.POST;
            request.AddJsonBody(obj);
            request.Timeout = 30000;// Timeout.Infinite;
            var result = client.Execute(request);

            if (result.StatusCode.ToString() == "OK")
            {
                string value = result.Content;//Sample: {"adHeaderID":173}

                JObject adIDJson = JObject.Parse(value);
                string adID = adIDJson["adHeaderStoreSuccess"] == null ? "" : adIDJson["adHeaderStoreSuccess"].ToString();//JsonConvert.DeserializeObject<string>(@"{""adHeaderID"":173}");

                return adID;
            }
            else
                return "0";
        }
    }
}
