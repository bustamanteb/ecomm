﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace eComm
{
    class CMS_DB
    {
        private string connectionString;
        private SqlConnection mssqlConnection;
        private SqlCommand mssqlCommand;

        public CMS_DB(string connString)
        {
            mssqlConnection = new SqlConnection(connString);
            mssqlCommand = new SqlCommand();
            mssqlCommand.Connection = mssqlConnection;
            mssqlCommand.CommandType = CommandType.StoredProcedure;
            //returnParameter = new SqlParameter();
        }

        public DataSet getSaveUpdateAds_eComm(string fromDate, string toDate, string description, string clientAdID)
        {
            try
            {
                SqlDataAdapter myMSsqlAdaptor = new SqlDataAdapter();
                DataSet myMSsqlDataSet = new DataSet();

                if (mssqlConnection.State == ConnectionState.Open)
                    mssqlConnection.Close();

                mssqlCommand.CommandText = "spSaveUpdateAds_eComm";
                mssqlCommand.Parameters.Clear();
                mssqlCommand.Parameters.AddWithValue("@FromDate", fromDate);
                mssqlCommand.Parameters.AddWithValue("@ToDate", toDate);
                mssqlCommand.Parameters.AddWithValue("@Description", description);
                mssqlCommand.Parameters.AddWithValue("@ClientAdID", clientAdID);
                mssqlConnection.Open();

                myMSsqlAdaptor.SelectCommand = mssqlCommand;
                int numOfRows = myMSsqlAdaptor.Fill(myMSsqlDataSet);
                return myMSsqlDataSet;
            }
            catch
            {
                return null;
            }
            finally
            {
                mssqlConnection.Close();
                mssqlCommand.Parameters.Clear();
            }
        }

        public DataSet saveAdHeaderStores_eComm(string adID, string storeID, string eCommXMLUrl)
        {
            try
            {
                SqlDataAdapter myMSsqlAdaptor = new SqlDataAdapter();
                DataSet myMSsqlDataSet = new DataSet();

                if (mssqlConnection.State == ConnectionState.Open)
                    mssqlConnection.Close();

                mssqlCommand.CommandText = "spSaveAdHeaderStores_eComm";
                mssqlCommand.Parameters.Clear();
                mssqlCommand.Parameters.AddWithValue("@AdID", adID);
                mssqlCommand.Parameters.AddWithValue("@StoreID", storeID);
                mssqlCommand.Parameters.AddWithValue("@eCommXMLUrl", eCommXMLUrl);
                mssqlConnection.Open();

                myMSsqlAdaptor.SelectCommand = mssqlCommand;
                int numOfRows = myMSsqlAdaptor.Fill(myMSsqlDataSet);
                return myMSsqlDataSet;
            }
            catch
            {
                return null;
            }
            finally
            {
                mssqlConnection.Close();
                mssqlCommand.Parameters.Clear();
            }
        }

        public DataSet saveAdHeaderStoresImages_eComm(string adID, string storeID, string imageURL, string imageName)
        {
            try
            {
                SqlDataAdapter myMSsqlAdaptor = new SqlDataAdapter();
                DataSet myMSsqlDataSet = new DataSet();

                if (mssqlConnection.State == ConnectionState.Open)
                    mssqlConnection.Close();

                mssqlCommand.CommandText = "spSaveAdHeaderStoresImages_eComm";
                mssqlCommand.Parameters.Clear();
                mssqlCommand.Parameters.AddWithValue("@AdID", adID);
                mssqlCommand.Parameters.AddWithValue("@StoreID", storeID);
                mssqlCommand.Parameters.AddWithValue("@ImageURL", imageURL);
                mssqlCommand.Parameters.AddWithValue("@ImageName", imageName);
                mssqlConnection.Open();

                myMSsqlAdaptor.SelectCommand = mssqlCommand;
                int numOfRows = myMSsqlAdaptor.Fill(myMSsqlDataSet);
                return myMSsqlDataSet;
            }
            catch
            {
                return null;
            }
            finally
            {
                mssqlConnection.Close();
                mssqlCommand.Parameters.Clear();
            }
        }
    }
}
