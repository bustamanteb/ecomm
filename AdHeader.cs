﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eComm
{
    class AdHeader
    {
        private string adID;
        private string startDate;
        private string endDate;
        private string description;
        private string clientAdID;

        public AdHeader(string startDate, string endDate, string description, string clientAdID)
        {
            this.startDate = startDate;
            this.endDate = endDate;
            this.description = description;
            this.clientAdID = clientAdID;
        }

        public AdHeader(string adID)
        {
            this.AdID = adID;
        }

        public string AdID { get { return adID; } set { adID = value; } }
        public string StartDate { get { return startDate; } }
        public string EndDate { get { return endDate; } }
        public string Description { get { return description; } }
        public string ClientAdID { get { return clientAdID; } }

        public string saveAdData(string clientDB)
        {
            MySQL mySql = new MySQL(clientDB);

            string connectionString = mySql.mssqlConnectionString(clientDB);

            CMS_DB cmsDB = new CMS_DB(connectionString);

            DataSet newAdheader = cmsDB.getSaveUpdateAds_eComm(startDate, endDate, description, clientAdID);

            if (newAdheader != null)
            {
                AdID = newAdheader.Tables[0].Rows[0]["AdID"].ToString(); //Get the new Ad Header AdID created
                return AdID;
            }
            else
                return "0";
        }

        public string saveAdData(string clientUsername, string clientPw)
        {
            RestClient client = new RestClient("https://cloud.brdata.com/api/Ecomm/");
            //RestClient client = new RestClient("http://saastest.brdata.com/api/Ecomm/");
            //RestClient client = new RestClient("http://localhost:57484/api/eComm/");        

            string obj = "{\"BaseParams\": [{\"username\": \"" + clientUsername + "\",\"password\": \"" + clientPw + "\",\"query\": \"" + "adHeader" + "\"}]," +
                           "\"QueryParams\": [{" +
                                "\"startDate\": \"" + startDate + "\"," +
                                "\"endDate\": \"" + endDate + "\"," +
                                "\"description\": \"" + description + "\"," +
                                "\"clientAdID\": \"" + clientAdID + "\"" +
                                "}]}";

            RestRequest request = new RestRequest();//("Products", Method.GET);
            request.Method = Method.POST;
            request.AddJsonBody(obj);
            request.Timeout = 30000;// Timeout.Infinite;
            var result = client.Execute(request);

            if (result.StatusCode.ToString() == "OK")
            {
                string id = result.Content;//Sample: {"adHeaderID":173}

                JObject adIDJson = JObject.Parse(id);
                string adID = adIDJson["adHeaderID"] == null ? "" : adIDJson["adHeaderID"].ToString();//JsonConvert.DeserializeObject<string>(@"{""adHeaderID"":173}");

                return adID;
            }
            else
                return "0";
        }
    }
}
