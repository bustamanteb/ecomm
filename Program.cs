﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace eComm
{
    class Program
    {        
        static void Main(string[] args)
        {
            string currentDir = "";

            decimal mins = Convert.ToDecimal(DateTime.Now.TimeOfDay.Minutes.ToString());

            string currentDateTime = "Date_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Year.ToString() + "_Time_" + DateTime.Now.TimeOfDay.Hours.ToString() + "_" + Math.Round(mins, 0).ToString();

            List<string> reportMessages = new List<string>();

            //Report message
            reportMessages.Add("BRData's eComm Message Report. Date of Report - " + DateTime.Now.ToShortDateString() + "\r\n");
            reportMessages.Add("----------------------------START----------------------------");


            if (args.Length != 4)
            {
                reportMessages.Add("Error: The process was halted.");
                reportMessages.Add("The following parameters was not provided: - XML and Zip path; - Client Name eg 'MaceysCMS';  - Client User Name eg 'client@clientdomain.com'; - Client User Name Password\r\n");
                reportMessages.Add("The following parameters was provided:");

                foreach (string parameter in args)
                    reportMessages.Add(" -'" + parameter + "'");

                reportMessages.Add("----------------------------END----------------------------");

                currentDir = Directory.GetCurrentDirectory();

                //Write Report Messages to txt File
                if (!System.IO.Directory.Exists(currentDir + @"\reports"))
                {
                    System.IO.Directory.CreateDirectory(currentDir + @"\reports");
                }

                using (StreamWriter outputFile = new StreamWriter(currentDir + @"\reports\eCommReport_" + currentDateTime + ".txt"))
                {
                    foreach (string message in reportMessages)
                    {
                        outputFile.WriteLine(message);
                        Console.WriteLine(message);
                    }
                }
                return;
            }

            //Get all xml and zip files in directory
            currentDir = args[0];// Directory.GetCurrentDirectory();
            string clientDB = args[1];// client DB name eg. "MaceysCMS";
            string clientUsername = args[2];
            string clientPw = args[3];

            string[] xmlFiles = Directory.GetFiles(currentDir, "*.xml");
            string[] zipFiles = Directory.GetFiles(currentDir, "*.zip");
            
            //Declaration of all variables needed
            string adID = "";
            string adDescription = "";
            string startDate = "";
            string endDate = "";
            string clientAdID = "";
            string storeNo = "";
            string eCommXMLUrl = "";
            string imgUrl = "";
            string imgName = "";                     

            Dictionary<string, string> xmlImgPairs = new Dictionary<string, string>();

            foreach (string xmlPath in xmlFiles)
            {
                string xmlFileName = Path.GetFileName(xmlPath);
                string[] xmlPathArray = Regex.Split(xmlFileName, "_");

                if (xmlPathArray.Length == 5 && xmlPathArray[4].ToLower() == "BRDATA.xml".ToLower()) //Sample Format - 20160518_MACEYSPRICELOCK2PAGEAD_67866_11304_BRDATA
                {
                    foreach (string zipPath in zipFiles)
                    {
                        string zipFileName = Path.GetFileName(zipPath);
                        string[] zipPathArray = Regex.Split(zipFileName, "_");

                        if (zipPathArray.Length == 5 && zipPathArray[4].ToLower() == "PROOF.zip".ToLower()) //Sample Format - 20160518_MACEYSPRICELOCK2PAGEAD_67866_11304_PROOF
                        {
                            if(xmlPathArray[0] == zipPathArray[0] && 
                                    xmlPathArray[1] == zipPathArray[1] &&
                                        xmlPathArray[2] == zipPathArray[2] &&
                                            xmlPathArray[3] == zipPathArray[3])
                            {
                                xmlImgPairs.Add(xmlFileName, zipFileName);
                            }
                        }                        
                    }
                }
            }

            //Report message
            reportMessages.Add("Number of xml & zip file pairs found: " + xmlImgPairs.Count + " (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")\r\n");

            foreach (KeyValuePair<string, string> pair in xmlImgPairs)
            {
                //Move Files to "working folders". These are the xml and img folders.
                string sourcePath = currentDir;
                string xmlFileName = pair.Key;
                string zipFileName = pair.Value;

                //Report message
                reportMessages.Add("\r\nStart of xml & zip file pair processing: " + xmlFileName + " : " + zipFileName + " (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")\r\n");

                /***************XML Files***************/
                string targetPath = currentDir + @"\xml";

                AdHeaderXML xmlFileObj = new AdHeaderXML(xmlFileName, sourcePath, targetPath);

                xmlFileObj.moveXMLFile(); //Moves XML file to "working" folder


                /***************Zipped Images***************/
                targetPath = currentDir + @"\img";

                AdHeaderZip zipFileObj = new AdHeaderZip(zipFileName, sourcePath, targetPath);

                zipFileObj.moveZipFile(); //Moves XML file to "working" folder

                zipFileObj.unzipImages(); //Unzip images to "working" folder


                /**********PARSE XML***********/
                string xmlFilePath = System.IO.Path.Combine(xmlFileObj.XmlTargetFilePath, xmlFileObj.XmlFile);

                string xmlAdHeaderData = xmlFileObj.openXMLFile(xmlFilePath);

                if (xmlAdHeaderData != "")
                {
                    xmlFileObj.parseXMLFile(xmlAdHeaderData, out startDate, out endDate, out adDescription, out clientAdID, out storeNo);

                    //Report message
                    reportMessages.Add(" - XML file successfully loaded. (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")");
                }
                else
                {
                    //Report message
                    reportMessages.Add(" - Either the XML file could not be read or is empty. Please review. (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")");
                }

                /**********PARSE XML********END*/

                //Validates that these properties has a value after parsing the xml document
                if (startDate != "" && endDate != "" && adDescription != "" && clientAdID != "" && storeNo != "")
                {
                    //Get all image names and paths from the "working" img folder
                    Dictionary<string, string> imgDictionary = zipFileObj.getImageNameAndPath();

                    //Report message
                    reportMessages.Add(" - XML file successfully parsed. (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")");

                    AdHeader adHeader = new AdHeader(startDate, endDate, adDescription, clientAdID);

                    adID = adHeader.saveAdData(clientUsername, clientPw);

                    int newAdID;

                    //If the Ad header wasnt saved the entire process is aborted
                    if (Int32.TryParse(adID, out newAdID) && newAdID != 0)
                    {
                        try
                        {
                            adHeader.AdID = newAdID.ToString();
                            
                            /**********UPLOAD XML CLOUD FILES************/
                            //CloudFiles cloudFiles = new CloudFiles("brdataweb", "5e78905850214aee8cb9f286552320ef"); //Cloud Files api login

                            string object_name_xml = clientDB + "/eComm/xml/" + xmlFileObj.XmlFile;

                            var fileStream = new FileStream(xmlFilePath, FileMode.Open, FileAccess.Read);
                            using (fileStream)
                            {
                                //cloudFiles.uploadObject(fileStream, "Ads", object_name_xml);
                                AzureCloudFiles.SaveFile(object_name_xml, "ads", fileStream);
                            }

                            //eCommXMLUrl = cloudFiles.objectUrl("Ads", object_name_xml); //Assign eComm XML Url 
                            eCommXMLUrl = AzureCloudFiles.FileUri(object_name_xml, "ads");

                            /**********UPLOAD XML CLOUD FILES********END*/

                            if (eCommXMLUrl != "")
                            {
                                //Report message
                                reportMessages.Add(" - XML file was successfully uploaded: " + eCommXMLUrl + " (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")");
                                
                                AdHeaderStores adHeaderStores = new AdHeaderStores(adHeader.AdID, storeNo, eCommXMLUrl);

                                string adHeaderStoresResponse = adHeaderStores.saveAdData(clientUsername, clientPw); //Save to DB

                                AdHeaderStoresImages adHeaderStoreImg;

                                string adHeaderStoresImagesResponse;

                                //Upload to cloud file and save to the database for each image
                                foreach (KeyValuePair<string, string> img in imgDictionary)
                                {
                                    string imageName = img.Key;
                                    string imagePath = img.Value;

                                    /**********UPLOAD IMG CLOUD FILES************/
                                    string object_name_img = clientDB + "/eComm/img/" + imageName;

                                    fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
                                    using (fileStream)
                                    {
                                        //cloudFiles.uploadObject(fileStream, "Ads", object_name_img);
                                        AzureCloudFiles.SaveFile(object_name_img, "ads", fileStream);
                                    }

                                    //imgUrl = cloudFiles.objectUrl("Ads", object_name_img); //Assign eComm IMG Url
                                    imgUrl = AzureCloudFiles.FileUri(object_name_img, "ads");

                                    /**********UPLOAD IMG CLOUD FILES********END*/

                                    //Delete Image from "working folder"
                                    File.Delete(imagePath);

                                    if (imgUrl != "")
                                    {
                                        //Report message
                                        reportMessages.Add(" - Image file: " + imageName + " was successfully uploaded: " + imgUrl + " (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")");

                                        adHeaderStoreImg = new AdHeaderStoresImages(adHeader.AdID, storeNo, imgUrl, imageName);

                                        adHeaderStoresImagesResponse = adHeaderStoreImg.saveAdData(clientUsername, clientPw); //Save to DB

                                        if (adHeaderStoresImagesResponse != "0")
                                        {
                                            //Report message
                                            reportMessages.Add(" - Ad Header Store Image information successfully saved. (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")");
                                        }
                                        else
                                        {
                                            //Report message
                                            reportMessages.Add(" - An error occured when trying to save the Ad Header Store Image information. Please review. (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")");
                                        }

                                    }

                                }

                                //Report message
                                reportMessages.Add(" - XML file successfully uploaded: " + eCommXMLUrl + " (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")");
                            }
                            else
                            {
                                //Report message
                                reportMessages.Add(" - An error occured. XML file was not uploaded. Please review. (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")");
                            }
                        }
                        catch (Exception e)
                        {
                            //Report message
                            reportMessages.Add(" - An error occured when uploading xml and images: " + e.Message + " (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")");
                        }
                    }
                    else
                    {
                        //Report message
                        reportMessages.Add(" - An error occured when trying to save the Ad Header information. Please review. (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")");
                    }
                }
                else
                {
                    //Report message
                    reportMessages.Add(" - One or more of the following could not be found in the xml file name or in its contents - Start Date, End Date, Ad Description, AdID and/or Store Number. Please review. (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")");
                }

                //Move files to archive folder
                try
                {                    
                    //A new folder in the Archive Folder is created for each time the entire process is ran. Unique by date.  
                    xmlFileObj.moveXmlToArchive(currentDir + @"\archiveFiles\eComm_" + currentDateTime);

                    zipFileObj.moveZipImgToArchive(currentDir + @"\archiveFiles\eComm_" + currentDateTime);

                    /*In case the user profile running the script doesnt have elevated privileges to delete the images, the images are moved to the archiveFiles folder*/
                    string[] files = Directory.GetFiles(targetPath);                    
                    
                    foreach (string file in files)
                    {
                        if (!System.IO.Directory.Exists(currentDir + @"\archiveFiles\eComm_" + currentDateTime + @"\" + zipFileName.Substring(0, (zipFileName.Length - 4))))
                        {
                            System.IO.Directory.CreateDirectory(currentDir + @"\archiveFiles\eComm_" + currentDateTime + @"\" + zipFileName.Substring(0, (zipFileName.Length - 4)));
                        }
                        
                        string fileName = Path.GetFileName(file);
                        string targetFile = currentDir + @"\archiveFiles\eComm_" + currentDateTime + @"\" + zipFileName.Substring(0, (zipFileName.Length - 4)) + @"\" + fileName;

                        System.IO.File.Move(file, targetFile);
                    }

                    //Report message
                    reportMessages.Add(" - XML and Zip files moved to Archive Folder. (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")");
                }
                catch (Exception e)
                {
                    //Report message
                    reportMessages.Add(" - An error has occured when trying to move the XML and Zip files to the Archive Folder. Please review: " + e.Message + " (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")");
                }

                //Report message
                reportMessages.Add("\r\nEnd of xml & zip file pair processing: " + xmlFileName + " : " + zipFileName + " (TimeStamp: " + DateTime.Now.ToShortTimeString() + ")\r\n\r\n");

            }

            reportMessages.Add("----------------------------END----------------------------");

            //Write Report Messages to txt File
            if (!System.IO.Directory.Exists(currentDir + @"\reports"))
            {
                System.IO.Directory.CreateDirectory(currentDir + @"\reports");
            }

            using (StreamWriter outputFile = new StreamWriter(currentDir + @"\reports\eCommReport_" + currentDateTime + ".txt"))
            {
                foreach (string message in reportMessages)
                {
                    outputFile.WriteLine(message);
                    Console.WriteLine(message);
                }
            }

            //Console.ReadLine();
        }
    }
}
