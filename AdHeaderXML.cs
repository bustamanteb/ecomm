﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace eComm
{
    class AdHeaderXML
    {
        private string xmlFile;
        private string xmlFilePath;
        private string xmlTargetFilePath;

        public AdHeaderXML(string xmlFile, string xmlFilePath, string xmlTargetFilePath)
        {
            this.xmlFile = xmlFile;
            this.xmlFilePath = xmlFilePath;
            this.xmlTargetFilePath = xmlTargetFilePath;
        }

        public string XmlFile { get { return xmlFile;} }
        public string XmlFilePath { get { return xmlFilePath; } }
        public string XmlTargetFilePath { get { return xmlTargetFilePath; } }

        //public string getWorkingXMLFileAndPath()
        //{
        //    return System.IO.Path.Combine(XmlTargetFilePath, XmlFile);
        //}

        public void moveXMLFile()
        {
            // To move a folder's contents to a new location:
            // Create a new target folder, if necessary.
            if (!System.IO.Directory.Exists(xmlTargetFilePath))
            {
                System.IO.Directory.CreateDirectory(xmlTargetFilePath);
            }

            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.Combine(XmlFilePath, XmlFile);
            string destFile = System.IO.Path.Combine(XmlTargetFilePath, XmlFile);

            // move xml file to working folder
            System.IO.File.Move(sourceFile, destFile);
        }

        public string openXMLFile(string xmlFile)
        {
            string xmlAdHeaderData = "";
            try
            {
                var fileStream = new FileStream(xmlFile, FileMode.Open, FileAccess.Read);
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    xmlAdHeaderData = streamReader.ReadToEnd();
                }
            }
            catch
            {

            }

            return xmlAdHeaderData;
        }

        public void parseXMLFile(string xmlAdHeaderData, out string startDate, out string endDate, out string adDescription, out string clientAdID, out string storeNo)
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(xmlAdHeaderData);

                //Removes the xml declaration from file is present : <?xml version="1.0" encoding="utf-8"?>
                foreach (XmlNode node in xml)
                {
                    if (node.NodeType == XmlNodeType.XmlDeclaration)
                    {
                        xml.RemoveChild(node);
                    }
                }

                XmlAttributeCollection storeNodeAttr = xml.FirstChild.Attributes; //Store node attributes

                XmlAttribute xmlStartDate = (XmlAttribute)storeNodeAttr.GetNamedItem("StartDate");
                XmlAttribute xmlEndDate = (XmlAttribute)storeNodeAttr.GetNamedItem("EndDate");
                XmlAttribute xmlAdName = (XmlAttribute)storeNodeAttr.GetNamedItem("AdName");

                startDate = xmlStartDate.Value; //Assign startDate
                endDate = xmlEndDate.Value; //Assign endDate
                adDescription = xmlAdName.Value; //Assign ad description
                clientAdID = ""; //Assign client ID
                storeNo = ""; //Assign store no

                string[] xmlNameArr = Regex.Split(XmlFile, "_");
                if (xmlNameArr.Length == 5)
                {
                    //adDescription = xmlNameArr[1]; //Assign ad description
                    clientAdID = xmlNameArr[2]; //Assign client ID
                    storeNo = xmlNameArr[3]; //Assign store no

                    //Remove first digit of store number
                    if (storeNo.Length == 5)
                        storeNo = storeNo.Substring(1, 4);

                    //Zero fill store no
                    while (storeNo.Length != 6)
                        storeNo = "0" + storeNo;
                }
            }
            catch
            {
                startDate = ""; //Assign startDate
                endDate = ""; //Assign endDate
                adDescription = ""; //Assign ad description
                clientAdID = ""; //Assign client ID
                storeNo = ""; //Assign store no
            }
        }

        public void moveXmlToArchive(string archiveDir)
        {
            // To move a folder's contents to a new location:
            // Create a new target folder, if necessary.
            if (!System.IO.Directory.Exists(archiveDir))
            {
                System.IO.Directory.CreateDirectory(archiveDir);
            }

            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.Combine(XmlTargetFilePath, XmlFile);
            string destFile = System.IO.Path.Combine(archiveDir, XmlFile);

            // move xml file from working folder to archive folder
            System.IO.File.Move(sourceFile, destFile);
        }
    }
}

//CODE REMOVED 
////Iterates through each 'Page' element in xml to get image name
//foreach (var img in imgFiles)
//{
//    //pageNodeAttr = xn.Attributes;
//    //XmlAttribute xmlImageName = (XmlAttribute)pageNodeAttr.GetNamedItem("ImageName");

//    //imgName = xmlImageName.Value; //Assign image name
//    imgList.Add(img); //Adds image names to list
//}