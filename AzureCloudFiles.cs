﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace eComm
{
    public class AzureCloudFiles
    {
        public static void UploadFile(string object_name, string path, string newFileName, Stream fileContents)
        {
            CloudStorageAccount storageAccount = null;
            CloudBlobContainer cloudBlobContainer = null;

            //Dictionary<string, string> connectionStringDict = CloudConnection.getConnectionStringInfo();

            string storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=brdatablobstorage;AccountKey=z6i8kYJDmJflG5X+nXaP0/KhfdW4IUCO7QZYz80K8lfhgk/C7xOZ0Po2oeGhuxw8LZEXz2DD/vjBD9wQJoZS/A==;EndpointSuffix=core.windows.net";

            // Check whether the connection string can be parsed.
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {

                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                cloudBlobContainer = cloudBlobClient.GetContainerReference("fileuploads");

                var blockBlob = cloudBlobContainer.GetBlockBlobReference(object_name);

                blockBlob.UploadFromStream(fileContents);

                newFileName = Path.Combine(path, object_name);

                blockBlob.DownloadToFile(newFileName, FileMode.Create);

            }
        }

        public static void SaveFile(string object_name, string container, Stream fileContents)
        {
            CloudStorageAccount storageAccount = null;
            CloudBlobContainer cloudBlobContainer = null;

            string storageConnectionString = "";

            if (container == "fileuploads" || container == "faileduploads")
            {
                storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=brdatablobstorage;AccountKey=z6i8kYJDmJflG5X+nXaP0/KhfdW4IUCO7QZYz80K8lfhgk/C7xOZ0Po2oeGhuxw8LZEXz2DD/vjBD9wQJoZS/A==;EndpointSuffix=core.windows.net";
            }
            else
            {
                storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=brdatacdn;AccountKey=zX3MmsSDin0W4hE/qOZ+7ABx9QQJQFmDTfnrpvavhIZcPNV9xO20p7MLpmCsArJYaaL1lev+77EChAs5XojlLg==;EndpointSuffix=core.windows.net";
            }
            // Check whether the connection string can be parsed.
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {

                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                cloudBlobContainer = cloudBlobClient.GetContainerReference(container);

                cloudBlobContainer.CreateIfNotExists();

                BlobContainerPermissions perm = cloudBlobContainer.GetPermissions();
                perm.PublicAccess = BlobContainerPublicAccessType.Blob;
                cloudBlobContainer.SetPermissions(perm);

                var blockBlob = cloudBlobContainer.GetBlockBlobReference(object_name);

                blockBlob.UploadFromStream(fileContents);

            }
        }

        public static void DeleteFile(string object_name, string container)
        {
            CloudStorageAccount storageAccount = null;
            CloudBlobContainer cloudBlobContainer = null;
            string storageConnectionString = "";

            //Dictionary<string, string> connectionStringDict = CloudConnection.getConnectionStringInfo();
            if (container == "fileuploads" || container == "faileduploads")
            {
                storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=brdatablobstorage;AccountKey=z6i8kYJDmJflG5X+nXaP0/KhfdW4IUCO7QZYz80K8lfhgk/C7xOZ0Po2oeGhuxw8LZEXz2DD/vjBD9wQJoZS/A==;EndpointSuffix=core.windows.net";
            }
            else
            {
                storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=brdatacdn;AccountKey=zX3MmsSDin0W4hE/qOZ+7ABx9QQJQFmDTfnrpvavhIZcPNV9xO20p7MLpmCsArJYaaL1lev+77EChAs5XojlLg==;EndpointSuffix=core.windows.net";
            }

            // Check whether the connection string can be parsed.
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {

                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                cloudBlobContainer = cloudBlobClient.GetContainerReference(container);

                var blockBlob = cloudBlobContainer.GetBlockBlobReference(object_name);

                blockBlob.Delete(DeleteSnapshotsOption.IncludeSnapshots);

            }
        }
        public static void MoveFile(string object_name, string fromContainer, string toContainer)
        {
            CloudStorageAccount storageAccount = null;

            //Dictionary<string, string> connectionStringDict = CloudConnection.getConnectionStringInfo();

            //string storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=brdatablobstorage;AccountKey=z6i8kYJDmJflG5X+nXaP0/KhfdW4IUCO7QZYz80K8lfhgk/C7xOZ0Po2oeGhuxw8LZEXz2DD/vjBD9wQJoZS/A==;EndpointSuffix=core.windows.net";
            string storageConnectionString = "";

            if (fromContainer == "fileuploads")
            {
                storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=brdatablobstorage;AccountKey=z6i8kYJDmJflG5X+nXaP0/KhfdW4IUCO7QZYz80K8lfhgk/C7xOZ0Po2oeGhuxw8LZEXz2DD/vjBD9wQJoZS/A==;EndpointSuffix=core.windows.net";
            }
            else
            {
                storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=brdatacdn;AccountKey=zX3MmsSDin0W4hE/qOZ+7ABx9QQJQFmDTfnrpvavhIZcPNV9xO20p7MLpmCsArJYaaL1lev+77EChAs5XojlLg==;EndpointSuffix=core.windows.net";
            }

            // Check whether the connection string can be parsed.
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {

                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                var sourceContainer = cloudBlobClient.GetContainerReference(fromContainer);

                var sourceBlob = sourceContainer.GetBlockBlobReference(object_name);

                var destinationContainer = cloudBlobClient.GetContainerReference(toContainer);

                var destinationBlob = destinationContainer.GetBlockBlobReference(object_name);

                destinationBlob.StartCopy(sourceBlob);

                sourceBlob.Delete(DeleteSnapshotsOption.IncludeSnapshots);

            }
        }
        public static string FileUri(string object_name, string container)
        {
            CloudStorageAccount storageAccount = null;
            CloudBlobContainer cloudBlobContainer = null;

            //Dictionary<string, string> connectionStringDict = CloudConnection.getConnectionStringInfo();

            //string storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=brdatablobstorage;AccountKey=z6i8kYJDmJflG5X+nXaP0/KhfdW4IUCO7QZYz80K8lfhgk/C7xOZ0Po2oeGhuxw8LZEXz2DD/vjBD9wQJoZS/A==;EndpointSuffix=core.windows.net";
            string storageConnectionString = "";

            if (container == "fileuploads" || container == "faileduploads")
            {
                storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=brdatablobstorage;AccountKey=z6i8kYJDmJflG5X+nXaP0/KhfdW4IUCO7QZYz80K8lfhgk/C7xOZ0Po2oeGhuxw8LZEXz2DD/vjBD9wQJoZS/A==;EndpointSuffix=core.windows.net";
            }
            else
            {
                storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=brdatacdn;AccountKey=zX3MmsSDin0W4hE/qOZ+7ABx9QQJQFmDTfnrpvavhIZcPNV9xO20p7MLpmCsArJYaaL1lev+77EChAs5XojlLg==;EndpointSuffix=core.windows.net";
            }

            // Check whether the connection string can be parsed.
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {

                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                cloudBlobContainer = cloudBlobClient.GetContainerReference(container);

                var blockBlob = cloudBlobContainer.GetBlockBlobReference(object_name);

                return blockBlob.Uri.AbsoluteUri;

            }
            else
            {
                return "File Not Found";
            }
        }
    }
}
